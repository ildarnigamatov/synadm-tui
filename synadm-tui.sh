#!/bin/bash

TOKEN=token
HOST=http://localhost:8008
DOMAIN=domain

mainMenu () {
  OPTION=$( whiptail --title  "Synapse Admin TUI" --menu  "Choose your option" 15 60 5 \
  "1" "Users" \
  "2" "Rooms" 3>&1 1>&2 2>&3 )

  if [ "$OPTION" = 1 ]; then 
    userMenu
  elif [ "$OPTION" = 2 ]; then
    roomsMenu
  fi
}

errMessege () {
  whiptail --title  "Error" --msgbox  "The menu item is not completed yet" 10 60
  mainMenu
}

userMenu () {
  OPTION=$( whiptail --title  "Synapse Admin TUI" --menu  "Choose your option" 15 60 5 \
  "1" "Add new user" \
  "2" "Reset password" \
  "3" "Deactivate user" \
  "4" "Rename" 3>&1 1>&2 2>&3 )

  if [ "$OPTION" = "1" ]; then 
   addUser
  elif [ "$OPTION" = 2 ]; then
    resetPassword
  elif [ "$OPTION" = 3 ]; then
    deactivateUser
  elif [ "$OPTION" = 4 ]; then
    rename
  fi
}

addUser () {
  local USER_ID=$(whiptail --title  "Add new user" --inputbox  "User ID" 10 60 login 3>&1 1>&2 2>&3)
  local DISPLAY_NAME=$(whiptail --title  "Add new user" --inputbox  "Display name" 10 60 Login 3>&1 1>&2 2>&3)
  local DISPLAY_NAME=$(echo $DISPLAY_NAME | sed "s/ /\\\u0020/g" ) #support for spaces in DISPLAY_NAME
  local PSWRD=$(whiptail --title  "Add new user" --passwordbox  "Enter password " 10 60 3>&1 1>&2 2>&3)
  curl --header "Authorization: Bearer $TOKEN" -X PUT $HOST/_synapse/admin/v2/users/@$USER_ID:$DOMAIN -d '{"password": "'$PSWRD'","displayname": "'$DISPLAY_NAME'"}'
  mainMenu
}

resetPassword () {
  local USER_ID=$(whiptail --title  "Reset password" --inputbox  "User ID" 10 60 login 3>&1 1>&2 2>&3)
  local PSWRD=$(whiptail --title  "Reset password" --passwordbox  "Enter password " 10 60 3>&1 1>&2 2>&3)
  curl --header "Authorization: Bearer $TOKEN" -X POST $HOST/_synapse/admin/v1/reset_password/@$USER_ID:$DOMAIN -d '{"new_password": "'$PSWRD'","logout_devices": "true"}'
  mainMenu
}

deactivateUser () {
  local USER_ID=$(whiptail --title  "Deactivate user" --inputbox  "User ID" 10 60 login 3>&1 1>&2 2>&3)
  curl --header "Authorization: Bearer $TOKEN" -X POST $HOST/_synapse/admin/v1/deactivate/@$USER_ID:$DOMAIN -d '{"erase": true}'
}

rename () {
  local USER_ID=$(whiptail --title  "Rename" --inputbox  "User ID" 10 60 login 3>&1 1>&2 2>&3)
  local DISPLAY_NAME=$(whiptail --title  "Rename" --inputbox  "Display name" 10 60 Login 3>&1 1>&2 2>&3)
  local DISPLAY_NAME=$(echo $DISPLAY_NAME | sed "s/ /\\\u0020/g" ) #support for spaces in DISPLAY_NAME
  curl --header "Authorization: Bearer $TOKEN" -X PUT $HOST/_synapse/admin/v2/users/@$USER_ID:$DOMAIN -d '{"displayname": "'$DISPLAY_NAME'"}'
  mainMenu
}

roomsMenu() {
  OPTION=$( whiptail --title  "Synapse Admin TUI" --menu  "Choose your option" 15 60 5 \
  "1" "Delete room" \
  "2" "Empty" 3>&1 1>&2 2>&3 )

  if [ "$OPTION" = 1 ]; then 
    deleteRoom
  elif [ "$OPTION" = 2 ]; then 
    errMessege
  fi
}

deleteRoom() {
  local ROOM_ID=$(whiptail --title  "Delete Room" --inputbox  "Room ID" 10 60 ID 3>&1 1>&2 2>&3)
  curl --header "Authorization: Bearer $TOKEN" -X DELETE $HOST/_synapse/admin/v1/rooms/$ROOM_ID -d '{"block": true,"purge": true}'
  mainMenu
}

mainMenu
